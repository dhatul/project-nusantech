import { PrettyChatWindow } from 'react-chat-engine-pretty';

const ChatsPage = (props) => {
    return (
        <div className="background">
            <div className='chat-wrapper'>
                <PrettyChatWindow
                    projectId="83f01e9a-f535-4729-9a1c-4e6b695c020f"
                    username={props.user.username}
                    secret={props.user.secret}
                />
            </div>
        </div>
    );
}

export default ChatsPage